#ifndef CONFIG_H__
#define CONFIG_H__

#include <string>
#include <memory>

struct Configuration {
	std::string host_;
	std::string port_;

	std::size_t workers_processes;
	std::size_t workers_connections;

	uint16_t broadcast_port_;
};

std::unique_ptr<Configuration>
parse_configuration_from_file(const std::string &path);

#endif // CONFIG_H__
