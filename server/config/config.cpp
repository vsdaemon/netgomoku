#include "config.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <memory>
#include <string>
#include <sstream>

#include "../log/log.h"

namespace bp = boost::property_tree;
static void parse_broadcast_from_ini(bp::ptree &ptree, Configuration *cfg)
{
	for (auto &tag: ptree.get_child("broadcast"))
	{
		if (tag.first == "port")
		{
			cfg->broadcast_port_ = tag.second.get<uint16_t>("");
		}
		else
		{
			std::stringstream errmsg;
			errmsg << "cannot parse broadcast config: \"" << tag.first
				<< "\" is invalid field" << std::endl;
			std::gomoku_log << errmsg.str();
			throw std::domain_error(errmsg.str());
		}
	}
}

static void parse_server_from_ini(bp::ptree &ptree, Configuration *cfg)
{
	for (auto &tag: ptree.get_child("server"))
	{
		if (tag.first == "host")
		{
			cfg->host_ =
				tag.second.get<std::string>("");
		}
		else if (tag.first == "port")
		{
			cfg->port_ =
				tag.second.get<std::string>("");
		}
		else if (tag.first == "workers_processes")
		{
			cfg->workers_processes =
				tag.second.get<std::size_t>("");
		}
		else if (tag.first == "workers_connections")
		{
			cfg->workers_connections =
				tag.second.get<std::size_t>("");
		}
		else
		{
			std::stringstream errmsg;
			errmsg << "cannot parse message: \"" << tag.first
				<< "\" is invalid field" << std::endl;
			std::gomoku_log << errmsg.str();
			throw std::domain_error(errmsg.str());
		}
	}
}

std::unique_ptr<Configuration>
parse_configuration_from_file(const std::string &path)
{
	std::unique_ptr<Configuration> cfg { new Configuration };

	bp::ptree ptree;
	bp::read_ini(path, ptree);

	parse_broadcast_from_ini(ptree, cfg.get());
	parse_server_from_ini(ptree, cfg.get());

	return cfg;
}
