#ifndef BROADCAST_SERVER__
#define BROADCAST_SERVER__

#include <boost/asio.hpp>
#include <boost/asio/yield.hpp>
#include <memory>

class BroadCastServer: public boost::asio::coroutine
{
	public:
		explicit BroadCastServer(boost::asio::io_service &service,
				uint16_t port, const std::string &host_info);

	public:
		void start_session(const boost::system::error_code &, int bytes);

	private:
		boost::asio::ip::udp::socket socket_;
		boost::asio::ip::udp::endpoint endpoint_;
		std::string host_info_;
		char buffer_[1024];
};

#endif 		// BROADCAST_SERVER
