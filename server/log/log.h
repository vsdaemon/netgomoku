#ifndef LOG_H__
#define LOG_H__

#include <sstream>
#include <fstream>
#include <string>
#include <mutex>
#include <boost/noncopyable.hpp>

enum LogLevel {
	WARN,
	DEBUG,
	INFO,
	ERROR,
	FATAL,
};

class Logger: public std::ostringstream, public boost::noncopyable
{
	public:
		explicit Logger(const char *f);
		explicit Logger(const std::string &f);
		virtual ~Logger();

		void set_level(LogLevel lvl);
		void flush();

		template <typename Type>
		Logger &operator<<(const Type &t);

		Logger &operator << (const LogLevel &lvl);
		typedef Logger &(*LoggerManip)(Logger &);
		Logger &operator << (LoggerManip m);

	private:
		std::string get_time() const;
		const char *level_str(const LogLevel &) const;

	private:
		std::mutex mutex_;
		std::ofstream file_;
		std::ostream log_;
		LogLevel level_;
		LogLevel line_level_;
};

template <typename Type>
Logger &Logger::operator<<(const Type &t)
{
	*static_cast<std::ostringstream*>(this) << t;
	return *this;
}

namespace std {
	inline Logger &endl(Logger &out)
	{
		out.put('\n');
		out.flush();
		return out;
	}
}

namespace std {
	extern Logger gomoku_log;
}

#endif          // LOG_H__
