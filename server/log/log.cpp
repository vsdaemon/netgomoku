#include "log.h"

#include <time.h>
#include <cstring>
#include <fstream>
#include <stdexcept>

namespace std {
	Logger gomoku_log { "gomoku_main.log" };
}

Logger::Logger(const char *f):
	file_(f, std::ios_base::app | std::ios_base::out),
	log_(file_.rdbuf()), level_(INFO), line_level_(INFO)
{
	if (!file_.is_open())
		throw std::runtime_error("cannot open log file");
}

Logger::Logger(const std::string &f):
	file_ { f.data(), std::ios_base::app | std::ios_base::out },
	log_(file_.rdbuf()), level_(INFO), line_level_(INFO)
{
	if (!file_.is_open())
		throw std::runtime_error("cannot open log file");
}

Logger::~Logger()
{
	file_.close();
}

void Logger::set_level(LogLevel lvl)
{
	level_ = lvl;
}

void Logger::flush()
{
	if (line_level_ >= level_) {
		log_ << get_time() << " -- [" << level_str(line_level_) << "] -- " << str();
		log_.flush();
	}
	str("");
}

Logger &Logger::operator<<(const LogLevel &lvl)
{
	line_level_ = lvl;
	return *this;
}

Logger &Logger::operator<<(LoggerManip m)
{
	return m(*this);
}

std::string Logger::get_time() const
{
	auto cur_time = time(0);
	char *time_buf = asctime(localtime(&cur_time));
	size_t len = std::strlen(time_buf);
	if (len > 0) time_buf[len - 1] = 0;
	return time_buf;
}

const char *Logger::level_str(const LogLevel &lvl) const {
	switch (lvl) {
		case WARN:
			return "WARNING";
		case DEBUG:
			return "DEBUG";
		case INFO:
			return "INFORMATION";
		case ERROR:
			return "ERROR";
		case FATAL:
			return "FATAL";
	}
	return "UNKNOWN LEVEL";
}
