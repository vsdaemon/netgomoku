#include "broadcast_server.h"

#include <boost/bind.hpp>
#include <cstring>

#include "log/log.h"

boost::asio::ip::udp::endpoint remote_endpoint;

BroadCastServer::BroadCastServer(boost::asio::io_service &service,
		uint16_t port, const std::string &host_info):
	socket_(service,boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), port)),
	host_info_ { host_info }
{
	std::gomoku_log << INFO << "broadcast_server: binded into [host: " <<
		"0.0.0.0, and port: " << port << "]\n";
	start_session(boost::system::error_code(), 0);
}

void BroadCastServer::start_session(const boost::system::error_code &ec, int bytes)
{
	auto len_msg = sizeof("gomoku.net ping") - 1;
	if (!ec)
	{
		reenter(this) for (;;)
		{
			std::gomoku_log << INFO <<
				"broadcast_server: initial callback for receive messages" << std::endl;
			yield socket_.async_receive_from(boost::asio::buffer(buffer_), endpoint_,
					boost::bind(&BroadCastServer::start_session, this,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred
			));

			if (std::strlen(buffer_) >= len_msg &&
					!std::strncmp(buffer_, "gomoku.net ping", len_msg))
			{
				std::gomoku_log << INFO <<
					"broadcast_server: received keyword: is, and initial callback for send" <<
					std::endl;
				yield socket_.async_send_to(boost::asio::buffer(host_info_), endpoint_,
						boost::bind(&BroadCastServer::start_session, this,
						boost::asio::placeholders::error,
						boost::asio::placeholders::bytes_transferred
				));
			}
		}
	}
	else
	{
		std::gomoku_log << ERROR <<
			"broadcast_server: error send messages <" << ec.message() << ">" << std::endl;
		throw std::runtime_error(ec.message());
	}
}
