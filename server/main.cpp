#include <iostream>
#include <stdexcept>

#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include <thread>

#include "log/log.h"
#include "config/config.h"
#include "broadcast_server.h"

enum
{
	SUCCESS = 0x000,
	ERROR_IN_COMAND_LINE = 0x001,
	ERROR_IN_UNHANDLED_EXCEPTION = 0x002,
	ERROR_PARSE_CONFIG_FILE = 0x003,
};

int main(int argc, char **argv) try
{
	std::string path_to_config = "gomoku.conf";
	namespace po = boost::program_options;
	po::options_description desc("Options");
	desc.add_options()
		("help,h", "Print help messages")
		("config,c", po::value<std::string>(&path_to_config), "Set path to config file");

	po::variables_map vm;
	try
	{
		po::store(po::parse_command_line(argc, argv, desc), vm);
		if (vm.count("help"))
		{
			std::cout << "Basic Command Line Parameter App" << std::endl
				<< desc << std::endl;
			return SUCCESS;
		}

		po::notify(vm);
	}
	catch (po::error &er)
	{
		std::gomoku_log << ERROR << er.what() << std::endl;

		std::cerr << "Error: " << er.what() << std::endl << std::endl;
		std::cerr << desc << std::endl;
		return ERROR_IN_COMAND_LINE;
	}

	try
	{
		std::unique_ptr<Configuration> configuration =
			parse_configuration_from_file(path_to_config);
	}
	catch (std::exception &e)
	{
		std::cerr << "Cannot parse configuration file: "
			<< e.what() << std::endl;
		return ERROR_PARSE_CONFIG_FILE;
	}

	boost::asio::io_service io_service;
	BroadCastServer serv(io_service, 1234, "localhost");
	serv.start_session(boost::system::error_code(), 0);
	boost::asio::signal_set signals(io_service, SIGINT, SIGTERM);
	signals.async_wait([&io_service](const boost::system::error_code &ec, int signo)
	{
		io_service.stop();
	});
	io_service.run();

	std::cout << "stoped" << std::endl;

	return SUCCESS;
}
catch (std::exception &e)
{
	std::cerr << "Unhandled exception reached the top of main: "
		<< e.what() << ", application wil now exit" << std::endl;
	return ERROR_IN_UNHANDLED_EXCEPTION;
}
